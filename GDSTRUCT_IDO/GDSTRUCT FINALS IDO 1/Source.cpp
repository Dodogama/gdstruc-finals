#include <iostream>
#include <string>
#include "Stack.h"
#include "Queue.h"
#include "UnorderedArray.h"

using namespace std;

int main()
{
	int elementSize;
	int choice;
	int number;

	cout << "'''''''''GDSTRUCT FINALS''''''''' " << endl;
	cout << "================================= " << endl << endl;
	cout << "Input size of elements: ";
	cin >> elementSize;
	cout << endl << endl;

	Queue<int> queue(elementSize);
	Stack<int> stack(elementSize);

	while (true)
	{
		cout << "What do you want to do? " << endl;
		cout << "[1] Push Back " << endl;
		cout << "[2] Pop Back " << endl;
		cout << "[3] Display elements and then Delete " << endl;
		cin >> choice;
		cout << endl;

		//Push for Queue and Stack
		if (choice == 1)
		{
			cout << "Input number: " << endl;
			cin >> number;
			queue.push(number);
			stack.push(number);
			cout << endl << endl;

			cout << "Top Element of Sets " << endl;
			cout << "Queue: ";
			cout << queue.top() << endl;
			cout << "Stack: ";
			cout << stack.top() << endl;
			cout << endl;

			system("pause");
		}

		//Pop
		if (choice == 2)
		{
			cout << "You've popped the front elements!!! " << endl << endl;
			queue.pop();
			stack.pop();
		}

		//Print and Delete all Elements
		if (choice == 3)
		{
			cout << "Queue Elements: " << endl;
			queue.printQueueElements();
			cout << endl;
			cout << "Stack Elements: " << endl;
			stack.printStackElements();
			cout << endl;
		}
	
	}
	system("pause");
	
	return 0;
}